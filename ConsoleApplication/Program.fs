﻿open System
open System.Text
open Console
open System.Linq

type Position = 
    struct 
        val Col : int
        val Row : int
        new(c, r) = { Col = c; Row = r }
    end

type TypedWordStatusWithCommonPartLen =
    | NotStatrted 
    | Valid of int
    | Invalid of int
    | Completed 

let typedWordValidation (wordTyped : string, wordToType : string) =
    let typedWordState = TypingEngine.GetInstance().GetTypedWordState(wordTyped, wordToType)
    match typedWordState.Status with
    | Core.TypedWordStatus.NotStarted -> NotStatrted
    | Core.TypedWordStatus.Valid -> Valid typedWordState.CommonParts.Length
    | Core.TypedWordStatus.Invalid -> Invalid typedWordState.CommonParts.Length
    | Core.TypedWordStatus.Completed -> Completed

type TextToType(textToType : string[]) =
    let currentlyTypedWordIdxRef = ref 0 
    let textToType = textToType
    let typedWord = StringBuilder()

    member this.GetTypedWord() =
        typedWord.ToString()

    member this.GetTypedWordLength() =
        typedWord.Length

    member this.ClearTypedWord() =
        typedWord.Clear() |> ignore

    member this.RemoveLastCharacterOfTypedWord() =
        if(typedWord.Length <= 1) then
            typedWord.Clear() |> ignore
        else
            typedWord.Remove(Math.Max(0, typedWord.Length-1), Math.Abs(typedWord.Length - typedWord.Length-1)) |> ignore

    member this.AppendToTypedWord(ch : char) =
        typedWord.Append(ch) |> ignore
        
    member this.GetAlreadyTypedText() =
        if currentlyTypedWordIdxRef.Value = 0 then
            String.Empty
        else
            textToType |> Seq.take currentlyTypedWordIdxRef.Value |> Seq.reduce(fun acc x -> acc + " " + x)

    member this.GetWordToType() = 
        if(currentlyTypedWordIdxRef.Value < textToType.Length) then 
            (textToType |> Seq.item currentlyTypedWordIdxRef.Value) + (if(currentlyTypedWordIdxRef.Value <> textToType.Length-1) then " " else String.Empty)
        else
            String.Empty

    member this.GetUntypedPartOfWordToType(typedLen : int) = 
        (textToType |> Seq.item currentlyTypedWordIdxRef.Value).[typedLen+1..]

    member this.GetRemainingText() = 
        if(currentlyTypedWordIdxRef.Value < textToType.Length-1) then
            textToType |> Seq.skip (currentlyTypedWordIdxRef.Value+1) |> Seq.reduce(fun acc x -> acc + " " + x)
        else 
            String.Empty

    member this.NextWord() =
        incr currentlyTypedWordIdxRef

type TypingGame(text : string[]) =
    let typingEngine = new Core.TypingEngine()
    let textToType = TextToType(text)
    let gameOn = ref true
    let _lock = new Object()
    let cw = ConsoleWriter()
    let textCharacterCount = (text |> Array.sumBy (fun x -> x.Length+1))-1

    let drawProgressBar() = 
        let charactersTyped = textToType.GetAlreadyTypedText().Length
        lock _lock (fun _ -> 
            let percentComplete = if charactersTyped > 0 then int(Math.Round(((double charactersTyped / double textCharacterCount)*100.), 2)) else 0
            cw.Write(sprintf "%s %s" (Seq.replicate 10 "▬" |> Seq.concat |> Seq.toArray |> String) (percentComplete.ToString()+"%"), Font.Color.Gray, (0, 1))
            cw.Write((sprintf "%s" ((Seq.replicate (int(percentComplete/10)) "▬" |> Seq.concat |> Seq.toArray |> String))), Font.Color.Green, (0, 1))
        )


    let onTimerTick(startTime : DateTime) = 
        let secondsElapsed = int ((DateTime.Now - startTime)).TotalSeconds
        if gameOn.Value = false || secondsElapsed < 1 then
            ()
        else
            let charactersTyped = textToType.GetAlreadyTypedText().Length
            let wpm = typingEngine.CalculateWpm(charactersTyped, secondsElapsed)

            lock _lock (fun _ -> 
                cw.Write(sprintf "WPM: %d             " (wpm), _position=(0, 0))
            )
            drawProgressBar()
    
    member this.GameLoop() =
        let textLine = 2

        let timer = new System.Timers.Timer(100)
        let startTime = Lazy<DateTime>(fun _ -> DateTime.Now)

        timer.Stop()
        timer.Elapsed.Add ( function(_) -> onTimerTick(startTime.Value))

        Console.SetCursorPosition(0, textLine)

        cw.Write(sprintf "%s%s" (textToType.GetWordToType()) (textToType.GetRemainingText()), Font.Color.Gray)

        while gameOn.Value = true do
            let alreadyTypedText = textToType.GetAlreadyTypedText()
            let typedWordLen = textToType.GetTypedWordLength()

            lock _lock (fun _ -> 
                cw.SetCursorPosition(
                    (if alreadyTypedText.Length>0 then alreadyTypedText.Length+typedWordLen+1 else typedWordLen), 
                    textLine + ((alreadyTypedText.Length+5) / Console.BufferWidth)
                )
                cw.ShowCursor()

                let characterTyped = Console.ReadKey(true).KeyChar

                cw.HideCursor()
                cw.SetCursorPosition(0, 2)

                if(timer.Enabled = false) then
                    timer.Start()

                let backspaceHit = characterTyped.Equals('\b')
                let ctrlBackspaceHit = characterTyped.Equals('\u007f')

                let enterHit = characterTyped.Equals('\r')
                if (enterHit) then
                    timer.Stop()
                    gameOn.Value <- false

                let additionalSpaceToClear = (Seq.replicate (typedWordLen) " ") |> Seq.toArray |> String.Concat 

                if backspaceHit then
                    textToType.RemoveLastCharacterOfTypedWord()
                else if ctrlBackspaceHit then
                    textToType.ClearTypedWord()
                else
                    textToType.AppendToTypedWord(characterTyped)

                let remainingText = textToType.GetRemainingText()
                if alreadyTypedText.Length > 0 then
                    cw.Write(alreadyTypedText + " ", Font.Color.Green)

                let typedWord = textToType.GetTypedWord()
                let wordToType = textToType.GetWordToType()
                let typedWordValidation = typedWordValidation(typedWord, wordToType)

                match typedWordValidation with 
                | NotStatrted -> 
                    cw.Write(sprintf "%s%s" wordToType remainingText, Font.Color.Gray)
                | Valid commonPartLen -> 
                    let untypedPartOfWord = textToType.GetUntypedPartOfWordToType(typedWord.Length-1)
                    cw.Write(typedWord, Font.Color.Green)
                    cw.Write(sprintf "%s %s" untypedPartOfWord remainingText, Font.Color.Gray)
                | Invalid commonPartLen ->
                    let untypedPartOfWord = textToType.GetUntypedPartOfWordToType(typedWord.Length-1)
                    let correctPart     = typedWord.[..commonPartLen-1]
                    let wrongPart       = wordToType.[commonPartLen..commonPartLen + (typedWord.Length-commonPartLen-1)]
                    let extraWrongPart  = if(typedWord.Length > wordToType.Length-1) then "\b" + typedWord.[wordToType.Length-1..] else String.Empty
                    let remainingPart   = sprintf "%s %s"      untypedPartOfWord remainingText
                    cw.Write(correctPart, Font.Color.Green)
                    cw.Write(wrongPart, Font.Color.Red)
                    cw.Write(extraWrongPart, Font.Color.Red)
                    cw.Write(remainingPart, Font.Color.Gray)
                | Completed -> 
                    textToType.NextWord()
                    let wordToType = textToType.GetWordToType()
                    let remainingText = textToType.GetRemainingText()
                    if wordToType.Length = 0 then
                        timer.Stop()
                        gameOn.Value <- false
                        drawProgressBar()
                    textToType.ClearTypedWord()
                    cw.Write(typedWord, Font.Color.Green)
                    cw.Write(sprintf "%s%s" wordToType remainingText, Font.Color.Gray)
                cw.Write(additionalSpaceToClear)
            )

[<EntryPoint>]
let main argv =
    Console.Clear()
    let typingEngine = new Core.TypingEngine()
    while true do 
        Console.ReadKey()
        let typingGame = new TypingGame(typingEngine.GetMostCommonEnglishWords(50) |> Seq.toArray)
        System.Threading.Thread.Sleep 100
        Console.Clear()
        System.Threading.Thread.Sleep 100
        typingGame.GameLoop()
    0