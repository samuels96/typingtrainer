﻿module Console
open System

module Font =
    type Color =
    | White
    | Gray
    | Red
    | Green

module Cursor = 
    type Visibility =
    | Hidden
    | Visible

type ConsoleWriter() =

    let writeLock = new Object()

    let setCursorVisibility = 
        fun visibility -> (
            match visibility with
            |  Cursor.Visibility.Hidden -> System.Console.CursorVisible <- false
            |  Cursor.Visibility.Visible -> System.Console.CursorVisible <- true
        )

    member this.ShowCursor = 
        fun () -> ( setCursorVisibility Cursor.Visibility.Visible )

    member this.HideCursor = 
        fun () -> ( setCursorVisibility Cursor.Visibility.Hidden )

    member this.SetCursorPosition =
        fun (position) -> ( System.Console.SetCursorPosition(fst position % System.Console.BufferWidth, snd position % System.Console.BufferHeight) )

    member this.Write(text : string, ?_fontColor : Font.Color, ?_position : (int*int))= 

        let originalCursorPosition = System.Console.GetCursorPosition().ToTuple()

        let fontColorChange = _fontColor.IsSome
        let positionChange = _position.IsSome

        let fontColor = defaultArg _fontColor Font.Color.White
        let position = defaultArg _position (fst originalCursorPosition, snd originalCursorPosition)

        let originalFontColor = System.Console.ForegroundColor
        let newFontColor =
            match fontColor with
            | Font.Color.White -> System.ConsoleColor.White
            | Font.Color.Gray -> System.ConsoleColor.Gray
            | Font.Color.Red -> System.ConsoleColor.Red
            | Font.Color.Green -> System.ConsoleColor.Green

        lock writeLock (fun _ ->
            if positionChange then ( this.HideCursor(); this.SetCursorPosition(fst position, snd position) )
            if fontColorChange then System.Console.ForegroundColor <- newFontColor
            System.Console.Write(text)
            if fontColorChange then System.Console.ForegroundColor <- originalFontColor
            if positionChange then ( this.SetCursorPosition(fst originalCursorPosition, snd originalCursorPosition); this.ShowCursor() )
        )
        
    

