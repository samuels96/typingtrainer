﻿module TypingEngine
    let private inst = Lazy<Core.TypingEngine>.Create(fun() -> new Core.TypingEngine())
    let GetInstance() = inst.Value
