﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Infrastructure
{
    internal static class SupabaseAccess
    {
        public static Supabase.Client ClientInstance;
        public static async Task Initialize()
        {
            dynamic credentials = JsonConvert.DeserializeObject(Properties.Resources._supabaseCredentials);
            var url = credentials!.Url;
            var key = credentials!.Key;

            await Supabase.Client.InitializeAsync(url, key);

            ClientInstance = Supabase.Client.Instance;
        }
    }
}
