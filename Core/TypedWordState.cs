﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core
{
    public enum TypedWordStatus
    {
        NotStarted,
        Valid,
        Invalid,
        Completed
    }

    public class TypedWordState
    {
        private TypedWordStatus status;
        public TypedWordStatus Status { get => status; }

        private string commonParts;
        public string CommonParts { get => commonParts; }

        public TypedWordState(TypedWordStatus state, string commonParts)
        {
            this.status = state;
            this.commonParts = commonParts;
        }
    }
}
