﻿namespace Core
{
    public class TypingEngine
    {
        public TypingEngine()
        {

        }

        public int CalculateWpm(double correctCharactersTyped, int secondsElapsed)
        {
            return (int)Math.Round((double)correctCharactersTyped * (60 / secondsElapsed) / 5);
        }

        public TypedWordState GetTypedWordState(string wordTyped, string wordToType)
        {
            if (wordTyped.Length.Equals(0))
                return new TypedWordState(TypedWordStatus.NotStarted, String.Empty);

            if (wordTyped.Length.Equals(wordToType.Length) && wordTyped.Equals(wordToType))
                return new TypedWordState(TypedWordStatus.Completed, wordToType);

            var commonParts = new string(wordToType.TakeWhile((ch, i) => { return i < wordTyped.Length && wordTyped[i] == ch; }).ToArray());

            if (wordTyped.Length > wordToType.Length)
                return new TypedWordState(TypedWordStatus.Invalid, commonParts);

            return commonParts.Count().Equals(wordTyped.Length) 
                ? new TypedWordState(TypedWordStatus.Valid, wordTyped)
                : new TypedWordState(TypedWordStatus.Invalid, commonParts);
        }

        public IEnumerable<string> GetMostCommonEnglishWords(int amount = 50)
        {
            return Properties.Resources.mostCommonWords_English.Split('\n').OrderBy(x => Guid.NewGuid()).Take(amount);
        }
    }
}