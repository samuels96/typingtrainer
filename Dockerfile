# syntax=docker/dockerfile:1
FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build-env    

WORKDIR /src
COPY ["ConsoleApplication/ConsoleApplication.fsproj", "ConsoleApplication/"]
COPY ["Core/Core.csproj", "Core/"]
RUN mkdir Core/Properties
RUN echo " " > Core/Properties/.supabaseCredentials.json
RUN dotnet restore "ConsoleApplication/ConsoleApplication.fsproj"
COPY . .
WORKDIR "/src/ConsoleApplication"
RUN dotnet build "ConsoleApplication.fsproj" -c Release -o /app/build

# Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:6.0
WORKDIR /src
COPY --from=build-env /app/build .
ENTRYPOINT ["dotnet", "ConsoleApplication.dll"]

